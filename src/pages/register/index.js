import React, {useState} from 'react';
import {
  Image,
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {BackSvg} from '../../assets/svgIcon';
import HyperlinkText from '../../component/hyperlinkText';
import axios from 'axios';
import * as navigation from '../../config/router/rootNavigation';

const Register = () => {
  const [email, setemail] = useState('');
  const [name, setname] = useState('');
  const [password, setpassword] = useState('');
  const [success, setSuccess] = useState(false);
  const header = {
    email: email,
    name: name,
    password: password,
  };
  const registerControl = () => {
    if (email && name && password) {
      axios
        .post('http://code.aldipee.com/api/v1/auth/register', header)
        .then(res => setSuccess(res.data.success))
        .catch(err => console.log(err));
    }
    if (success) {
      return navigation.navigate('RegisterComplete');
    }
  };
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <BackSvg width={30} height={30} nav={'Login'} />
        <View style={styles.imageWrapper}>
          <Image
            style={styles.image}
            source={require('../../assets/images/loginImage.jpg')}
          />
        </View>
        <View>
          <TextInput
            onChangeText={text => setname(text)}
            value={name}
            style={styles.input}
            placeholder={'masukkan name'}
          />
          <TextInput
            onChangeText={text => setemail(text)}
            value={email}
            style={styles.input}
            placeholder={'masukkan email'}
          />
          <TextInput
            onChangeText={text => setpassword(text)}
            value={password}
            style={styles.input}
            secureTextEntry={true}
            placeholder={'masukkan password'}
          />
          <Pressable style={styles.button} onPress={() => registerControl()}>
            <Text style={styles.textButton}>Register</Text>
          </Pressable>
        </View>
        <HyperlinkText text={'Login'} nav={'Login'} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    margin: '3%',
  },
  image: {
    width: undefined,
    height: undefined,
    flex: 1,
    resizeMode: 'cover',
  },
  imageWrapper: {
    width: '100%',
    height: '35%',
    marginTop: '6%',
    marginBottom: '4%',
  },
  input: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    padding: 10,
    marginVertical: 10,
    borderRadius: 8,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    alignItems: 'center',
    elevation: 10,
    marginVertical: 10,
    borderRadius: 8,
  },
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
});

export default Register;
