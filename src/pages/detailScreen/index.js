import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Image,
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  Alert,
  View,
} from 'react-native';
import PushNotification from 'react-native-push-notification';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import {useNetInfo} from '@react-native-community/netinfo';
import NotifService from '../../component/notif/NotifService';
import {showMessage} from 'react-native-flash-message';
import Share from 'react-native-share';
import {useDispatch, useSelector} from 'react-redux';
import {BackSvg, LoveSvg, ShareSvg} from '../../assets/svgIcon';
import Button from '../../component/button';
import {
  formatCurrency,
  getSupportedCurrencies,
} from 'react-native-format-currency';

const Detail = props => {
  const netInfo = useNetInfo();
  const dataDetail = useSelector(state => state.reducerApi.dataDetail);
  const token = useSelector(state => state.reducerApi.token);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [result, setResult] = useState('');
  const onNotif = notif => {
    Alert.alert(notif.title, notif.message);
  };
  const notif = new NotifService(onNotif);
  const checkInternet = () =>
    netInfo.isConnected !== true
      ? showMessage({
          message: 'please connect your internet',
          type: 'connection error',
          backgroundColor: 'red',
          color: 'white',
        })
      : null;
  function getErrorString(error, defaultValue) {
    let e = defaultValue || 'Something went wrong. Please try again';
    if (typeof error === 'string') {
      e = error;
    } else if (error && error.message) {
      e = error.message;
    } else if (error && error.props) {
      e = error.props;
    }
    return e;
  }
  const shareSingleImage = async () => {
    const shareOptions = {
      title: 'Share Book Success',
      message: 'Share Book Success',
      failOnCancel: false,
    };

    try {
      const ShareResponse = await Share.open(shareOptions);
      setResult(JSON.stringify(ShareResponse, null, 2));
    } catch (error) {
      console.log('Error =>', error);
      setResult('error: '.concat(getErrorString(error)));
    }
  };

  const uang = parseInt(dataDetail.price);
  const [valueFormattedWithSymbol, valueFormattedWithoutSymbol, symbol] =
    formatCurrency({amount: uang, code: 'EUR'});

  const header = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  useEffect(() => {
    checkInternet();
    axios
      .get(`http://code.aldipee.com/api/v1/books/${props.idCard}`, header)
      .then(res => {
        dispatch({type: 'SAVE-DETAIL', payload: res.data});
        setLoading(false);
      })
      .catch(err =>
        showMessage({
          message: 'cannot connect to server',
          type: 'server error',
          backgroundColor: 'red',
          color: 'white',
        }),
      );
  }, []);
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <SkeletonContent
          containerStyle={{flex: 1, width: '100%'}}
          isLoading={loading}
          boneColor="white"
          highlightColor="#333333"
          animationDirection="horizontalLeft"
          layout={[
            {width: '94%', height: '30%', marginBottom: 6, margin: '3%'},
            {width: '94%', height: '10%', marginBottom: 6, margin: '3%'},
            {width: '94%', height: '40%', marginBottom: 6, margin: '3%'},
          ]}
          animationType="pulse">
          <View style={styles.card}>
            <View style={styles.navigasiIcon}>
              <BackSvg nav="Home" width={25} height={25} />
              <Text style={{fontWeight: 'bold', fontSize: 20}}>
                Detail Book
              </Text>
              <View style={styles.navigasiIconRight}>
                <Pressable onPress={() => notif.localNotif()}>
                  <LoveSvg width={25} height={25} />
                </Pressable>
                <View style={{margin: '3%'}}></View>
                <Pressable onPress={shareSingleImage}>
                  <ShareSvg width={25} height={25} />
                </Pressable>
              </View>
            </View>
            <View style={styles.mainCard}>
              <Image
                style={styles.cardImage}
                source={{uri: dataDetail.cover_image}}
              />
              <View style={styles.textCard}>
                <Text style={styles.judulBuku}>{dataDetail.title}</Text>
                <Text style={styles.authorBuku}>
                  Author By: {dataDetail.author}
                </Text>
                <Text style={styles.publisherBuku}>Publisher: </Text>
                <Text style={styles.publisherBuku}>{dataDetail.publisher}</Text>
                <Text style={styles.usersRating}>Users Rating</Text>
                <View style={styles.starRating}>
                  <Image
                    style={{width: 24, height: 24}}
                    source={{
                      uri: 'https://img.icons8.com/fluency/48/000000/star.png',
                    }}
                  />
                  <Image
                    style={{width: 24, height: 24}}
                    source={{
                      uri: 'https://img.icons8.com/fluency/48/000000/star.png',
                    }}
                  />
                  <Image
                    style={{width: 24, height: 24}}
                    source={{
                      uri: 'https://img.icons8.com/fluency/48/000000/star.png',
                    }}
                  />
                  <Image
                    style={{width: 24, height: 24}}
                    source={{
                      uri: 'https://img.icons8.com/fluency/48/000000/star.png',
                    }}
                  />
                  <Image
                    style={{width: 20, height: 20}}
                    source={{
                      uri: 'https://img.icons8.com/ios/50/000000/star--v1.png',
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.card2}>
            <View style={styles.ratingTextWrapper}>
              <Text style={{fontWeight: 'bold'}}>Rating</Text>
              <View style={styles.ratingNumber}>
                <Text>{dataDetail.average_rating}</Text>
                <Image
                  style={{width: 16, height: 16}}
                  source={{
                    uri: 'https://img.icons8.com/fluency/48/000000/star.png',
                  }}
                />
              </View>
            </View>
            <View style={styles.downloaded}>
              <Text style={{fontWeight: 'bold'}}>Downloaded</Text>
              <Text>{dataDetail.total_sale}</Text>
            </View>
            <Button title={'Rp. ' + valueFormattedWithoutSymbol} />
          </View>
          <ScrollView>
            <View style={styles.card3}>
              <Text style={styles.overview}>Overview</Text>
              <Text>{dataDetail.synopsis}</Text>
            </View>
          </ScrollView>
        </SkeletonContent>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    height: '100%',
  },
  navigasiIcon: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  navigasiIconRight: {
    flexDirection: 'row',
  },
  cardImage: {
    width: '40%',
    height: '100%',
    resizeMode: 'stretch',
  },
  card: {
    margin: '3%',
    padding: '3%',
    marginTop: 20,
    borderWidth: 1,
    backgroundColor: 'white',
    borderColor: 'white',
    elevation: 10,
  },
  judulBuku: {
    fontWeight: 'bold',
    fontSize: 17,
    maxWidth: '85%',
  },
  authorBuku: {
    fontSize: 14,
    marginVertical: 8,
  },
  publisherBuku: {
    fontSize: 16,
  },
  mainCard: {
    width: '100%',
    flexDirection: 'row',
  },
  textCard: {
    marginLeft: '4%',
  },
  usersRating: {
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 10,
  },
  starRating: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ratingNumber: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  card2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: '3%',
    padding: '2%',
    borderWidth: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    borderColor: 'white',
    elevation: 5,
  },
  downloaded: {
    marginLeft: 20,
    alignItems: 'center',
  },
  card3: {
    marginHorizontal: '3%',
    marginTop: '3%',
    marginBottom: '3%',
    padding: '2%',
    borderWidth: 1,
    backgroundColor: 'white',
    borderColor: 'white',
    elevation: 5,
  },
  ratingTextWrapper: {
    alignItems: 'center',
  },
  overview: {
    fontWeight: 'bold',
    fontSize: 20,
  },
});

export default Detail;
