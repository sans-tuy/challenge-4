import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import axios from 'axios';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import {useNetInfo} from '@react-native-community/netinfo';
import {showMessage} from 'react-native-flash-message';
import {useDispatch, useSelector} from 'react-redux';
import CardImage from '../../component/cardImage';
import CardImage2 from '../../component/cardImage2';
import PopulerBook from '../../component/populerBook/home';
import {ScrollView} from 'react-native-gesture-handler';

const Home = props => {
  const dataApi = useSelector(state => state.reducerApi.data);
  const token = useSelector(state => state.reducerApi.token);
  const dispatch = useDispatch();
  const netInfo = useNetInfo();
  const [loading, setLoading] = useState(true);
  const checkInternet = () =>
    netInfo.isConnected !== true
      ? showMessage({
          message: 'please connect your internet',
          type: 'connection error',
          backgroundColor: 'red',
          color: 'white',
        })
      : null;
  const sorting = (a, b) => {
    return b.average_rating - a.average_rating;
  };
  const header = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  useEffect(() => {
    checkInternet();
    axios
      .get('http://code.aldipee.com/api/v1/books', header)
      .then(res => {
        dispatch({type: 'SAVE-DATA', payload: res.data.results});
        setLoading(false);
      })
      .catch(err => console.log(err));
  }, []);
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <SkeletonContent
          containerStyle={{flex: 1, width: '100%'}}
          isLoading={loading}
          boneColor="white"
          highlightColor="#333333"
          animationDirection="horizontalLeft"
          layout={[
            {width: 220, height: 20, marginBottom: 6, marginTop: 10},
            {
              flexDirection: 'row',
              marginRight: 10,
              children: [
                {
                  width: 160,
                  height: 120,
                  marginBottom: 10,
                  marginRight: 20,
                },
                {
                  width: 160,
                  height: 120,
                  marginBottom: 10,
                  marginRight: 20,
                },
                {
                  width: 160,
                  height: 120,
                  marginBottom: 10,
                },
              ],
            },
            {width: 220, height: 20, marginBottom: 6},
            {width: '100%', height: 180, marginBottom: 6},
            {width: '100%', height: 180, marginBottom: 6},
            {width: '100%', height: 180, marginBottom: 6},
            {width: '100%', height: 180, marginBottom: 6},
          ]}
          animationType="pulse">
          <Text style={styles.greeting}>Good morning, Aldi!</Text>
          <Text style={styles.recomend}>Recommended</Text>
          <View style={styles.topCard}>
            <ScrollView horizontal>
              {dataApi.sort(sorting).map(item => {
                return (
                  <CardImage2
                    key={item.id}
                    idCard={item.id}
                    image={item.cover_image}
                  />
                );
              })}
            </ScrollView>
          </View>
          <PopulerBook title={'Populer Book'}>
            {dataApi
              .sort(sorting)
              .slice(0, 6)
              .map(item => {
                return (
                  <CardImage
                    key={item.id}
                    author={item.author}
                    publisher={item.publisher}
                    idCard={item.id}
                    judulBuku={item.title}
                    rating={item.average_rating}
                    price={item.price}
                    image={item.cover_image}
                  />
                );
              })}
          </PopulerBook>
        </SkeletonContent>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    height: '100%',
    margin: '3%',
  },
  cardWrapper: {
    flexWrap: 'wrap',
  },
  topCard: {
    flexDirection: 'row',
  },
  recomend: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  greeting: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default Home;
