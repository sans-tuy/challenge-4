import React, {useState} from 'react';
import {
  Image,
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import * as navigation from '../../config/router/rootNavigation';
import HyperlinkText from '../../component/hyperlinkText';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import {setDataApi} from '../../config/redux/action';

const Login = () => {
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');
  const token = useSelector(state => state.reducerApi.token);
  const dispatch = useDispatch();
  const header = {
    email: email,
    password: password,
  };
  const loginControl = () => {
    if (email && password) {
      setTimeout(() => {
        dispatch(setDataApi(header));
        if (token) {
          console.log('sukses', token);
          return navigation.navigateData('Home');
        }
      }, 3000);
    }
  };
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.imageWrapper}>
          <Image
            style={styles.image}
            source={require('../../assets/images/loginImage.jpg')}
          />
        </View>
        <View>
          <TextInput
            onChangeText={text => setemail(text)}
            value={email}
            style={styles.input}
            placeholder={'masukkan email'}
          />
          <TextInput
            onChangeText={text => setpassword(text)}
            value={password}
            style={styles.input}
            secureTextEntry={true}
            placeholder={'masukkan password'}
          />
          <Pressable style={styles.button} onPress={() => loginControl()}>
            <Text style={styles.textButton}>Login</Text>
          </Pressable>
        </View>
        <HyperlinkText text="Register" nav="Register" />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    margin: '3%',
  },
  image: {
    width: undefined,
    height: undefined,
    flex: 1,
    resizeMode: 'cover',
  },
  imageWrapper: {
    width: '100%',
    height: '35%',
    marginTop: '6%',
    marginBottom: '4%',
  },
  input: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    padding: 10,
    marginVertical: 10,
    borderRadius: 8,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    alignItems: 'center',
    elevation: 10,
    marginVertical: 10,
    borderRadius: 8,
  },
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
});

export default Login;
