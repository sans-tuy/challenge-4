import React from 'react';
import {Pressable, StyleSheet, Text} from 'react-native';
import * as RootNavigation from '../../config/router/rootNavigation';

const Button = props => {
  return (
    <Pressable
      style={styles.container}
      onPress={() => RootNavigation.navigate(props.nav)}>
      <Text style={styles.text}>{props.title}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'blue',
    padding: 10,
    alignItems: 'center',
    elevation: 10,
    marginVertical: 10,
    borderRadius: 8,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
});

export default Button;
