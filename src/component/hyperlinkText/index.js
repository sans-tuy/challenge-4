import React from 'react';
import {Text, StyleSheet, View, Pressable} from 'react-native';
import * as RootNavigation from '../../config/router/rootNavigation';

const HyperlinkText = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.text1}>Dont have an account ?</Text>
      <Pressable>
        <Text
          onPress={() => RootNavigation.navigate(props.nav)}
          style={styles.hyperlink}>
          {props.text}
        </Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  text1: {
    color: 'grey',
  },
  text2: {
    color: 'black',
  },
  hyperlink: {
    textDecorationLine: 'underline',
    fontWeight: 'bold',
  },
});

export default HyperlinkText;
