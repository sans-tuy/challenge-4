import React from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';

const PopulerBook = props => {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Text style={styles.text}>{props.title}</Text>
        <ScrollView>
          <View style={styles.cardWrapper}>{props.children}</View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  cardWrapper: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingTop: 10,
    paddingBottom: 250,
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default PopulerBook;
