import React, {useState, useEffect} from 'react';
import {Text, StyleSheet, View, Image, Pressable} from 'react-native';
import {useSelector} from 'react-redux';
import * as RootNavigation from '../../config/router/rootNavigation';

const CardImage = props => {
  return (
    <Pressable
      style={styles.container}
      onPress={() =>
        RootNavigation.navigateData('Detail', {idCard: props.idCard})
      }>
      <View style={{alignItems: 'center'}}>
        <View style={styles.imageWrapper}>
          <Image source={{uri: props.image}} style={styles.image} />
        </View>
        <Text style={styles.judulBuku}>{props.judulBuku}</Text>
      </View>
      <View style={{alignItems: 'center'}}>
        <Text style={styles.author}>Author: {props.author}</Text>
        <Text>Publisher: {props.publisher}</Text>
        <Text>Price: Rp. {props.price}</Text>
        <View style={styles.ratingNumber}>
          <Text>Rating: {props.rating}</Text>
          <Image
            style={{width: 16, height: 16}}
            source={{
              uri: 'https://img.icons8.com/fluency/48/000000/star.png',
            }}
          />
        </View>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '43%',
    marginBottom: 10,
    borderWidth: 1,
    elevation: 2,
  },
  image: {
    width: undefined,
    height: undefined,
    flex: 1,
  },
  judulBuku: {
    fontWeight: 'bold',
    fontSize: 14,
  },
  author: {
    fontSize: 14,
    textAlign: 'left',
  },
  imageWrapper: {
    width: '98%',
    height: 120,
    resizeMode: 'cover',
  },
  ratingNumber: {
    flexDirection: 'row',
  },
});

export default CardImage;
