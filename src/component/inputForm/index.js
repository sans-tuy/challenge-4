import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

const InputForm = props => {
  return (
    <>
      <TextInput
        secureTextEntry={props.isPass}
        style={styles.container}
        placeholder={props.hintInput}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    padding: 10,
    marginVertical: 10,
    borderRadius: 8,
  },
});

export default InputForm;
