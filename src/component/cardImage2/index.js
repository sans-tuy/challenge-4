import React from 'react';
import {StyleSheet, View, Image} from 'react-native';

const CardImage2 = props => {
  return (
    <View style={styles.container}>
      <Image source={{uri: props.image}} style={styles.image} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  image: {
    width: 120,
    height: 160,
    marginRight: 10,
    resizeMode: 'cover',
  },
});

export default CardImage2;
