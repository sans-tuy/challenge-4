import React, {useState, useCallback} from 'react';
import {RefreshControl, ScrollView} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../../pages/home';
import Detail from '../../pages/detailScreen';
import Splash from '../../pages/splashScreen';
import Register from '../../pages/register';
import Login from '../../pages/login';
import {navigationRef} from './rootNavigation';
import RegisterComplete from '../../pages/registerComplete';

const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

const HomeScreen = () => {
  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(1000).then(() => setRefreshing(false));
  }, []);
  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <Home />
    </ScrollView>
  );
};
const DetailScreen = ({route}) => {
  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(1000).then(() => setRefreshing(false));
  }, []);
  const {idCard} = route.params;
  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <Detail idCard={idCard} />
    </ScrollView>
  );
};
const SplashScreen = ({navigation}) => {
  return <Splash navigation={navigation} />;
};
const RegisterScreen = () => {
  return <Register />;
};
const LoginScreen = () => {
  return <Login />;
};
const RegisterCompleteScreen = () => {
  return <RegisterComplete />;
};

const Stack = createNativeStackNavigator();

const MainApp = () => {
  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Detail"
        component={DetailScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={RegisterScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="RegisterComplete"
        component={RegisterCompleteScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const Routing = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="Main"
          component={MainApp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Splash"
          component={SplashScreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routing;
