const initialState = {
  data: [],
  nama: 'sanusi',
  dataDetail: [],
  token: '',
};

const reducerApi = (state = initialState, action) => {
  switch (action.type) {
    case 'SAVE-DATA':
      return {...state, data: action.payload};

    case 'SAVE-DETAIL':
      return {...state, dataDetail: action.payload};

    case 'SAVE-TOKEN':
      return {...state, token: action.payload};

    default:
      return state;
  }
};

export default reducerApi;
