import axios from 'axios';
import {useDispatch} from 'react-redux';

export const setDataApi = header => {
  return dispatch =>
    axios
      .post('http://code.aldipee.com/api/v1/auth/login', header)
      .then(res =>
        dispatch({
          type: 'SAVE-TOKEN',
          payload: res.data.tokens.access.token,
        }),
      )
      .catch(err => console.log(err));
};
