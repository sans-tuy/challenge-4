import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';
import reducerApi from './reducer';

const allReducers = combineReducers({
  reducerApi: reducerApi,
});

const store = createStore(allReducers, applyMiddleware(thunk));

export default store;
