/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import FlashMessage from 'react-native-flash-message';
import Routing from './src/config/router';
import {Provider} from 'react-redux';
import store from './src/config/redux/store';

const App = () => {
  return (
    <Provider store={store}>
      <Routing />
      <FlashMessage position="top" />
    </Provider>
  );
};

export default App;
